elixi.re v2 API Documentation
===============

There are two types of tokens, timed (tokens) and non-timed (API keys).

API keys should be used for your uploading program (KShare, sharenix, etc.).
Timed tokens should be used for the frontend (they expire in 3 days).

All tokens should be given in the `Authorization` header of your request.
It does not matter which token you put in, both types will be in the same header.

The entire API uses JSON as input and output, assume that unless stated otherwise in a route.

## Status Codes

On a non-500 error, you will get a error payload.

It is JSON with an `error` field (boolean, True),
and a `message` field (str)

### Known error codes
 - `400`
   - Bad Request, your input data is non-standard.
 - `403`
   - Failed Authentication, your token failed verification,
        or your username and password combo failed.
 - `404`
   - Resource not found, with resource meaning a file or a shorten.
 - `420`
   - Banned from the service. A reason is in the `reason` field.
 - `429`
   - Ratelimited. Please wait `retry-after` seconds before calling again.
 - `503`
   - That part of the API is disabled in the instance.

### Upload-specific error codes
 - `415`
   - Bad Image.
 - `412`
   - Bad Upload. No files were given.
 - `469`
   - Quota Exploded. Can mean that your quota already exploded or
        *is about to* explode. Read the `message` field.

## Authentication

### `POST /api/login`
 - Should be the default way of logging-in, frontend wise.
 - Input: Receives JSON payload with `user` and `password` as fields.
 - Returns JSON with a `token` field, a timed token as value.

### `POST /api/apikey`
 - Main way to retrieve an api key for uploaders.
 - Input: Same as `POST /api/login`
 - Returns JSON with an `api_key` field, an API key as value.

### `POST /api/revoke`
 - Revoke all the user's current tokens.
 - All old tokens will become useless and invalid.
 - Input: Same as `POST /api/login`
 - Returns: JSON with an `success` field, should be always `true`.

## Profile


### `GET /api/domains`
 - Lists domains available to the authenticated user (if no authorization is supplied, only returns non-admin domains)
 - Optional authentication
 - Returns: JSON with `domains` array set to an array with the domain id pointing at the the domain they represent (note: the IDs might jump in numbers [as seen in the example] as there might be deleted or admin-only domains).
 - Example Output:

```javascript
{
    "domains": {
        // maps domain id to domain strings
        "0": "localhost.localdomain",
        "1": "elixi.re",
        "3": "127.0.0.1"
    },
    // domains owned by instance, not
    // donations
    "officialdomains": [1, 3]
}
```


### `GET /api/profile`
 - Get your basic account information
 - Requires authentication
 - Input: None
 - Returns: JSON with many keys with user info and limits
 - Example: 

```javascript
{
    "user_id":"410159602408230912", // Your user ID, a snowflake
    "username":"lunae", // Your username
    "active":true, // If this account is active or not
    "admin":false, // If you are an admin account
    "domain":0 // Your current domain ID. (the domain you are using for your images)
    "subdomain":"uwu" // Your current subdomain name
    "email":"owo@bama.example" // Your email address on record
    "paranoid":false, // If you are on paranoid mode or not
    "limits": {
        "limit": 104857600, // Weekly upload limit in bytes
        "used": null, // Bytes uploaded this week
        "shortenlimit": 100, // Weekly shortened link limit
        "shortenused": 0 // Shortens used this week
    }
}
```

   
### `PATCH /api/profile`
 - Modify your profile information.
 - Keep in mind changing your password **will invalidate all your tokens.**
 - Requires authentication with any token / API key.
 - Input fields, which are *all optional*:
   - `password`, string
   - `new_password`, requires `password`
   - `username`, string
   - `domain`, integer, represents the domain ID
   - `subdomain`, string from 0 to 63 caracters length.
   - `shorten_domain`, integer, represents the domain ID *used specifically for shortening*.
      - can be `null`, if you want to use the same domain for uploads and shortens.
   - `shorten_subdomain`, string from 0 to 63 characters length.
   - `email`, string
   - `consented`, boolean
     - `null`: user should get their data processing consent
     - `true`: user consented
     - `false`: user did not consent
   - `paranoid`, boolean
     - `true`: user will get 8+ chars of shortname on image upload
     - `false`: user will get 3+ chars of shortname on image upload
   - Example:

```javascript
{
    "password": "my old password",
    "new_password": "my new password",
    "domain": 0,
    "subdomain": "i-love",
    "consent": true,
    "paranoid": true,
    "email": "owo@wowo.example"
}
```
 - Returns: `updated_fields` field
 - Example:

```javascript
{
    // A list of things you have updated successfully in your profile
    // Any of those strings can appear.
    "updated_fields": ["password", "domain", "subdomain", "email", "consented"]
}
```


### `GET /api/limits`
 - Get your weekly upload limit information.
 - Requires authentication
 - Input: None
 - Returns JSON with `limit` key, the `used` key, as bytes (the unit of measurement), `shortenlimit` and `shortenused` keys, as link shorten counts. 


### `DELETE /api/account`
 - Make a request for account deletion.
 - Sends an email to your account asking for confirmation
 - Input: `password` field
 - Output: `success` field, boolean


### `POST /api/reset_password`
 - Make a request for password resetting.
 - Sends an email to your account.
 - No authentication.
 - Input: `username` field
 - Output: same as `DELETE /api/account`

## Upload


### `POST /api/upload`
 - Requires authentication
 - Upload files
 - Admins can set `admin` get value to a python-truthy value to bypass checks, non-admins setting this to truthy values will result in a 403
 - Only receives multipart formdata, nothing else, any field, only one file.
 - Returns: `url` and `shortname` fields


## Shorten


### `POST /api/shorten`
 - Shorten links
 - Requires authentication
 - Input: JSON with `url` key, containing the URL to shorten
 - Returns: JSON with `url` key, containing the shortened URL


## Files

### `DELETE /api/delete`
 - Deletes files
 - Requires authentication
 - Input: JSON with `filename` key set to the filename without extension (`abc`, not `abc.png`)
 - Returns: JSON with `success` key set to `true` (with HTTP 200), or JSON with `error` set to the error's details (with HTTP 404). 


### `GET /api/list`
 - Lists files of the authenticated user
 - Requires authentication
 - Returns: JSON with `success` key set to `true`, with `files` array set to a dictionary with name being the file's `shortname`, pointing at a dictionary of multiple values such as `snowflake`, `shortname`, `size` (in bytes), `thumbnail` and `url`. (Note: please just check the example below)
 - `shortens` array set to a dictionary with name being the shorten's `shortname`, pointing at a dictionary of multiple values such as `snowflake`, `shortname`, `redirto` and `url`. (Note: please just check the example below)
 - Note: `files` and `shortens` arrays might be empty. The files and shortens are by their shortnames. If you want to sort by time, you can use snowflakes.
 - Example Output:

```javascript
{
    "success": true,
    "files": {
        "6qw": {
            "snowflake": 422702060048355328,
            "shortname": "6qw",
            "size": 2133829,
            "url": "https://elixi.re/i/6qw.jpg",
            "thumbnail": "https://elixi.re/t/s6qw.jpg"
        },
        "ybj": {
            "snowflake": 422553767079186433,
            "shortname": "ybj",
            "size": 31606,
            "url": "https://s.ave.zone/i/ybj.png",
            "thumbnail": "https://elixi.re/t/sybj.jpg"
        }
    },
    "shortens": {
        "uec": {
            "snowflake": 422558731360931840,
            "shortname": "uec",
            "redirto": "https://example.com",
            "url": "https://elixi.re/s/uec"
        },
        "w9h": {
            "snowflake": 422558457124753408,
            "shortname": "w9h",
            "redirto": "https://elixi.re",
            "url": "https://stale-me.me/s/w9h"
        }
    }
}
```

### `DELETE /api/shortendelete`
 - Deletes shortened links
 - Requires authentication
 - Input: JSON with `filename` key set to the shorten filename (if shortened URL is `https://elixi.re/s/abc`, filename is `abc`)
 - Returns: JSON with `success` key set to `true` (with HTTP 200), or JSON with `error` set to the error's details (with HTTP 404). 


## Data Dump


### `POST /api/dump/request`
 - Requests a data dump to be delivered to your email.
 - No input.
 - Returns: `success` boolean field.

### `GET /api/dump/status`
 - Check the current status of your dump request if it is being processed.
 - No input.
 - Returns:
   - Object with a `state` key being any of: `"in_queue"`, `"not_in_queue"` or `"processing"`

   - If the `state` key is `"processing"`, those other keys appear
      - `start_timestamp`: string, timestamp of when the dump generation started(ISO 8601)
      - `current_id`: string(snowflake), the current file ID that the dump is working on
      - `total_files`, `files_done`: both integers, represent the total amount of files to work, and the current amount of files processed

   - If the `state` key is `"in_queue"`, another key: `"position"` will be there, with the current position of the user in the takeout queue.

### `GET /api/admin/dump_status`
 - Administrator only.
 - Returns:
   - `queue`: list of string(snowflake), the current users in the dump queue in order of request
   - `current`: dict(can be empty dict) with keys described on the `current_dump_state` table.
